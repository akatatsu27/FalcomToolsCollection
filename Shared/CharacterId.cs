﻿namespace SkySaveEditor;

public enum CharacterId
{
	Estelle = 0,
	Joshua = 1,
	Scherazard = 2,
	Olivier = 3,
	Kloe = 4,
	Agate = 5,
	Tita = 6,
	Zin = 7,
	None = 255,
}

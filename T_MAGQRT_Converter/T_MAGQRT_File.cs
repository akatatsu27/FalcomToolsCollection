﻿using System.Diagnostics;
using System.Text;
using Newtonsoft.Json;
using Shared;

namespace T_MAGQRT_Converter;

public class T_MAGQRT_File : FileClass
{
	[JsonIgnore] public ushort[] OffsetTable;
	public ArtsQuartzData[] Data;

	static T_MAGQRT_File()
	{
		Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
		ShiftJis = Encoding.GetEncoding("shift-jis");
	}
	private static readonly Encoding ShiftJis;
	public T_MAGQRT_File() { }

	public static async Task<T_MAGQRT_File> FromFile(string filepath)
	{
		T_MAGQRT_File file = new();
		await file.Parse(filepath);
		return file;
	}

	private async Task Parse(string filepath)
	{
		Bytes = await File.ReadAllBytesAsync(filepath);
		Coverage = new bool[Bytes.Length];

		List<ushort> offsets = new();
		ushort curOffset = 0;
		offsets.Add(ReadUInt16(ref curOffset));
		while (curOffset < offsets[0])
		{
			offsets.Add(ReadUInt16(ref curOffset));
		}
		Debug.Assert(offsets.IsSorted());
		Data = new ArtsQuartzData[offsets.Count];
		for (int i = 0; i < offsets.Count; i++)
		{
			Data[i] = new(ref curOffset, this);
		}
		Debug.Assert(IsFullCoverage);
	}

	/// <summary>
	/// Preserves the order of appearance
	/// </summary>
	/// <returns></returns>
	public async Task<byte[]> ToByteArray()
	{
		OffsetTable = new ushort[Data.Length];
		ushort curOffset = (ushort)(OffsetTable.Length * sizeof(ushort));
		for (int i = 0; i < Data.Length; i++)
		{
			OffsetTable[Data[i].ArtID] = curOffset;
			curOffset += 16;
		}
		await using MemoryStream ms = new();
		using BinaryWriter bw = new(ms, ShiftJis);
		for (int i = 0; i < OffsetTable.Length; i++)
			bw.Write(OffsetTable[i]);
		for (int i = 0; i < Data.Length; i++)
			bw.Write(Data[i].ToByteArray());
		byte[] asBytes = ms.ToArray();
		bw.Close();
		return asBytes;
	}
}
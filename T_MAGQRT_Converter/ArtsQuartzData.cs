﻿using Newtonsoft.Json;
using Shared;

namespace T_MAGQRT_Converter;

public class ArtsQuartzData
{
	public ushort ArtID;
	public ushort Earth;
	public ushort Water;
	public ushort Fire;
	public ushort Wind;
	public ushort Time;
	public ushort Space;
	public ushort Mirage;

	public ArtsQuartzData() { }

	public ArtsQuartzData(ref ushort curOffset, FileClass mS_File)
	{
		ArtID = mS_File.ReadUInt16(ref curOffset);
		Earth = mS_File.ReadUInt16(ref curOffset);
		Water = mS_File.ReadUInt16(ref curOffset);
		Fire = mS_File.ReadUInt16(ref curOffset);
		Wind = mS_File.ReadUInt16(ref curOffset);
		Time = mS_File.ReadUInt16(ref curOffset);
		Space = mS_File.ReadUInt16(ref curOffset);
		Mirage = mS_File.ReadUInt16(ref curOffset);
	}

	public unsafe byte[] ToByteArray()
	{
		byte[] byteArray = new byte[16];
		fixed (byte* bytes = byteArray)
		{
			*(ushort*)bytes = ArtID;
			*(ushort*)&bytes[2] = Earth;
			*(ushort*)&bytes[4] = Water;
			*(ushort*)&bytes[6] = Fire;
			*(ushort*)&bytes[8] = Wind;
			*(ushort*)&bytes[10] = Time;
			*(ushort*)&bytes[12] = Space;
			*(ushort*)&bytes[14] = Mirage;
		}
		return byteArray;
	}
}
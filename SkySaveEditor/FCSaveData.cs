﻿namespace SkySaveEditor;

public class FCSaveData
{
	public uint Mira;
	public ushort BracerRank;

	public unsafe FCSaveData(byte* data)
	{
	}
}

public class BattleStats
{
	public ushort Count;
	public ushort Lost;
	public ushort Won;
	public ushort Fled;
}

/// <summary>
/// Offset: 0x25C90
/// </summary>
public class Sepith
{
	public uint Earth;
	public uint Water;
	public uint Fire;
	public uint Wind;
	public uint Time;
	public uint Space;
	public uint Mirage;

	public unsafe Sepith(byte* data_ptr)
	{
		byte* ptr = data_ptr + 0x25C90;
		Earth = *(uint*)(&ptr);
		Water = *(uint*)(&ptr + 4);
		Fire = *(uint*)(&ptr + 8);
		Wind = *(uint*)(&ptr + 12);
		Time = *(uint*)(&ptr + 16);
		Space = *(uint*)(&ptr + 20);
		Mirage = *(uint*)(&ptr + 24);
	}
}